import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

public class FeatureGenerator {

	static String[] strings;
	static List<String> featureNames;
	static List<String> containNames;

	private static FastVector featureValue;
	private static FastVector labels;

	private static final String datasetName = "Badges";
	private static final String labelName = "Label";
	private static final String longerName = "Longer";
	private static final String equalName = "Equal";

	private static Instances instances;

	static {
		strings = new String[] { "First", "Last" };
		featureNames = new ArrayList<String>();
		containNames = new ArrayList<String>();

		for (String string : strings) {
			for (int position = 1; position <= 5; ++position) {
				for (char letter = 'a'; letter <= 'z'; ++letter) {
					featureNames.add("String" + "=" + string + "," + "Position" + "="
							+ position + "," + "Character" + "=" + letter);
				}
				// add special character
				featureNames.add("String" + "=" + string + "," + "Position" + "="
						+ position + "," + "Character" + "=" + "?");
			}
		}

		for (char letter = 'a'; letter <= 'z'; ++letter) {
			containNames.add("firstContains" + "=" + letter);
		}
		for (char letter = 'a'; letter <= 'z'; ++letter) {
			containNames.add("lastContains" + "=" + letter);
		}

		featureValue = new FastVector(2);
		featureValue.addElement("1");
		featureValue.addElement("0");

		labels = new FastVector(2);
		labels.addElement("+");
		labels.addElement("-");
	}

	private static void populateFeatures(Set<String> instFeatures, String string,
			char[] name) {
		int length = name.length > 5 ? 5 : name.length;
		for (int position = 1; position <= length; ++position) {
			instFeatures.add("String" + "=" + string + "," + "Position" + "="
					+ position + "," + "Character" + "=" + name[position - 1]); // 0...length-1
		}
		if (length < 5) {
			for (int position = length + 1; position <= 5; ++position) {
				instFeatures.add("String" + "=" + string + "," + "Position" + "="
						+ position + "," + "Character" + "=" + "?");
			}
		}
	}

	private static Set<String> populateContainFeatures(String firstName,
			String lastName) {
		Set<String> containFeatures = new HashSet<String>();
		for (char letter = 'a'; letter <= 'z'; ++letter) {
			String letterString = String.valueOf(letter);
			if (firstName.contains(letterString)) {
				containFeatures.add("firstContains" + "=" + letterString);
			}
			if (lastName.contains(letterString)) {
				containFeatures.add("lastContains" + "=" + letterString);
			}
		}
		return containFeatures;
	}

	private static Instance createInstance(String input) {
		input = input.trim();
		String[] inputParts = input.split("\\s+");

		String label = inputParts[0];
		char[] firstName = inputParts[1].toCharArray();
		char[] lastName = inputParts[2].toCharArray();

		Instance instance = new Instance(featureNames.size() + 2 + containNames.size() + 1); // +1 for label
		instance.setDataset(instances);

		Set<String> instFeatures = new HashSet<String>();
		populateFeatures(instFeatures, "First", firstName);
		populateFeatures(instFeatures, "Last", lastName);

		for (String featureName : featureNames) {
			Attribute attr = instances.attribute(featureName);
			String attrName = attr.name();
			String featureVal;
			if (instFeatures.contains(attrName)) {
				featureVal = "1";
			} else {
				featureVal = "0";
			}
			instance.setValue(attr, featureVal);
		}

		String longerVal = firstName.length > lastName.length ? "1" : "0";
		Attribute longerAttr = instances.attribute(longerName);
		instance.setValue(longerAttr, longerVal);

		String equalVal = firstName.length == lastName.length ? "1" : "0";
		Attribute equalAttr = instances.attribute(equalName);
		instance.setValue(equalAttr, equalVal);

		Set<String> containFeatures = populateContainFeatures(
				String.valueOf(firstName), String.valueOf(lastName));
		for (String containName : containNames) {
			Attribute containAttr = instances.attribute(containName);
			String containAttrName = containAttr.name();
			String containVal;
			if (containFeatures.contains(containAttrName)) {
				containVal = "1";
			} else {
				containVal = "0";
			}
			instance.setValue(containAttr, containVal);
		}

		instance.setClassValue(label);

		return instance;
	}

	private static void initAttributes() {
		FastVector attributes = new FastVector();

		for (String featureName : featureNames) {
			attributes.addElement(new Attribute(featureName, featureValue));
		}

		Attribute longer = new Attribute(longerName, featureValue);
		attributes.addElement(longer);

		Attribute equal = new Attribute(equalName, featureValue);
		attributes.addElement(equal);

		for (String containName : containNames) {
			attributes.addElement(new Attribute(containName, featureValue));
		}

		Attribute label = new Attribute(labelName, labels);
		attributes.addElement(label);

		instances = new Instances(datasetName, attributes, 0);
		instances.setClass(label);
	}

	private static Instances readData(String fileName) throws Exception {
		initAttributes();
		Scanner scanner = new Scanner(new File(fileName));
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			Instance instance = createInstance(line);
			instances.add(instance);
		}
		scanner.close();
		return instances;
	}

	public static void main(String[] args) throws Exception {
		if (args.length != 2) {
			System.err
					.println("Usage: FeatureGenerator input-badges-file features-file");
			System.exit(-1);
		}

		Instances data = readData(args[0]);
		ArffSaver saver = new ArffSaver();
		saver.setInstances(data);
		saver.setFile(new File(args[1]));
		saver.writeBatch();
	}
}