import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import weka.classifiers.Evaluation;
import weka.core.Instance;
import weka.core.Instances;
import cs446.weka.classifiers.trees.Id3;

public class Tester {

	private static Instances createTrainingFold(List<Instances> folds, int testIdx) {
		Instances trainingFold = null;
		for (int foldIdx = 0; foldIdx < folds.size(); ++foldIdx) {
			if (foldIdx != testIdx) {
				Instances insts = folds.get(foldIdx);
				if (trainingFold == null) {
					trainingFold = new Instances(insts);
				} else {
					for (int instIdx = 0; instIdx < insts.numInstances(); ++instIdx) {
						trainingFold.add(insts.instance(instIdx));
					}
				}
			}
		}
		return trainingFold;
	}

	private static List<Double> stumpsToVector(List<Id3> stumps, Instance inst)
			throws Exception {
		List<Double> vector = new ArrayList<Double>();
		vector.add(1.0); // add first element
		for (Id3 stump : stumps) {
			double classification = stump.classifyInstance(inst);
			vector.add(classification);
		}
		String label = inst.stringValue(inst.classIndex());
		if (label.equals("+")) {
			vector.add(1.0);
		} else {
			vector.add(-1.0);
		}
		return vector;
	}

	private static void initRandomly(List<Double> vector, int size) {
		Random random = new Random(System.currentTimeMillis());
		for (int vecIdx = 0; vecIdx < size; ++vecIdx) {
			vector.add(random.nextDouble());
		}
	}

	private static void initWZeros(List<Double> vector, int size) {
		for (int vecIdx = 0; vecIdx < size; ++vecIdx) {
			vector.add(0.0);
		}
	}

	private static double getVectorLength(List<Double> vector) {
		double length = 0;
		for (Double vecComp : vector) {
			length += Math.pow(vecComp, 2);
		}
		length = Math.sqrt(length);
		return length;
	}

	private static List<Double> instToVector(Instance inst) {
		List<Double> vector = new ArrayList<Double>();
		vector.add(1.0); // add first element
		for (int attrIdx = 0; attrIdx < inst.numAttributes() - 1; ++attrIdx) {
			String attrVal = inst.stringValue(attrIdx);
			if (attrVal.equals("1")) {
				vector.add(1.0);
			} else { // "0"
				vector.add(0.0);
			}
		}
		String label = inst.stringValue(inst.classIndex());
		if (label.equals("+")) {
			vector.add(1.0);
		} else {
			vector.add(-1.0);
		}
		return vector;
	}

	private static double getDotProduct(List<Double> vector1, List<Double> vector2) {
		double dotProduct = 0;
		for (int vecIdx = 0; vecIdx < vector1.size(); ++vecIdx) {
			dotProduct += (vector1.get(vecIdx) * vector2.get(vecIdx));
		}
		return dotProduct;
	}

	private static void update(List<Double> vector1, List<Double> vector2) {
		for (int vecIdx = 0; vecIdx < vector1.size(); ++vecIdx) {
			vector1.set(vecIdx, vector1.get(vecIdx) + vector2.get(vecIdx));
		}
	}

	private static List<Double> getTreeResults(List<Evaluation> evalSamples,
			List<Id3> treeSamples) {
		int bestIdx = 0;
		double bestAcc = 0;
		double accSum = 0;
		for (int i = 0; i < evalSamples.size(); ++i) {
			Evaluation evalSample = evalSamples.get(i);
			double acc = evalSample.correct() / evalSample.numInstances();
			if (acc > bestAcc) {
				bestAcc = acc;
				bestIdx = i;
			}
			accSum += acc;
		}
		double sampleMeanAccuracy = accSum / evalSamples.size();

		double diffSum = 0;
		for (Evaluation evalSample : evalSamples) {
			diffSum += Math.pow((evalSample.correct() / evalSample.numInstances())
					- sampleMeanAccuracy, 2);
		}
		double sampleStdDev = Math.sqrt(diffSum / (evalSamples.size() - 1));

		List<Double> results = new ArrayList<Double>();
		results.add(sampleMeanAccuracy);
		results.add(sampleStdDev);

		printBestFold(evalSamples.get(bestIdx), treeSamples.get(bestIdx), bestIdx);
		
		return results;
	}

	private static List<Double> getResults(List<Double> samples) {
		double accSum = 0;
		for (Double sample : samples) {
			accSum += sample;
		}
		double sampleMeanAccuracy = accSum / samples.size();

		double diffSum = 0;
		for (Double sample : samples) {
			diffSum += Math.pow(sample - sampleMeanAccuracy, 2);
		}
		double sampleStdDev = Math.sqrt(diffSum / (samples.size() - 1));

		List<Double> results = new ArrayList<Double>();
		results.add(sampleMeanAccuracy);
		results.add(sampleStdDev);

		return results;
	}

	private static void printBestFold(Evaluation bestFold, Id3 bestTree,
			int bestFoldIdx) {
		System.out.println("Test fold: " + bestFoldIdx);
		System.out.println(bestFold.correct() + " correct predictions");
		System.out.println(bestFold.incorrect() + " incorrect predictions");
		System.out.println("Tree:");
		System.out.println(bestTree);
		System.out.println();
	}

	private static List<Double> trainSGD(List<List<Double>> trainingVectors,
			double convThres, double learnRate) {
		List<Double> w = new ArrayList<Double>();
		List<Double> wDelta = new ArrayList<Double>();
		initRandomly(w, trainingVectors.get(0).size() - 1); // -1 for label
		initWZeros(wDelta, trainingVectors.get(0).size() - 1);
		int numExamples = 0;
		while (true) {
			for (List<Double> trainingVector : trainingVectors) {
				double labelVal = trainingVector.get(trainingVector.size() - 1);
				double fVal = getDotProduct(w, trainingVector);
				for (int deltaIdx = 0; deltaIdx < wDelta.size(); ++deltaIdx) {
					double compVal = trainingVector.get(deltaIdx);
					double deltaVal = learnRate * (labelVal - fVal) * compVal;
					wDelta.set(deltaIdx, deltaVal);
				}
				update(w, wDelta);
				/*System.out.println(getVectorLength(wDelta));
				System.out.println(convThres);
				System.out.println(numExamples);
				System.out.println();*/
				++numExamples;
				if (numExamples == 100) {
					if (getVectorLength(wDelta) < convThres) {
							return w;
					}
					numExamples = 0;
				}
			}
		}
	}

	private static List<Double> testSGD(List<Double> w,
			List<List<Double>> testVectors) {
		List<Double> result = new ArrayList<Double>(Arrays.asList(0.0, 0.0));
		for (List<Double> testVector : testVectors) {
			double labelVal = testVector.get(testVector.size() - 1);
			double fVal = getDotProduct(w, testVector);
			if (labelVal * fVal > 0) { // correct
				result.set(0, result.get(0) + 1); // num correct
			}
			result.set(1, result.get(1) + 1); // num total
		}
		return result;
	}

	public static List<Instances> initialize(String[] foldFiles) throws Exception {
		List<Instances> folds = new ArrayList<Instances>();
		for (String foldFile : foldFiles) {
			folds.add(new Instances(new FileReader(new File(foldFile))));
		}
		for (Instances fold : folds) {
			fold.setClassIndex(fold.numAttributes() - 1);
		}
		return folds;
	}

	public static List<Instances> createTrainingFolds(List<Instances> folds) {
		List<Instances> trainingFolds = new ArrayList<Instances>();
		for (int testIdx = 0; testIdx < folds.size(); ++testIdx) {
			trainingFolds.add(createTrainingFold(folds, testIdx));
		}
		return trainingFolds;
	}

	public static List<Instances> createTestFolds(List<Instances> folds) {
		List<Instances> testFolds = new ArrayList<Instances>();
		for (Instances insts : folds) {
			testFolds.add(insts);
		}
		return testFolds;
	}

	public static List<List<List<Double>>> genVectors(List<Instances> folds) {
		List<List<List<Double>>> vectorFolds = new ArrayList<List<List<Double>>>();
		for (Instances insts : folds) {
			List<List<Double>> vectors = new ArrayList<List<Double>>();
			for (int instIdx = 0; instIdx < insts.numInstances(); ++instIdx) {
				Instance inst = insts.instance(instIdx);
				List<Double> vector = instToVector(inst);
				vectors.add(vector);
			}
			vectorFolds.add(vectors);
		}
		return vectorFolds;
	}

	public static List<List<Id3>> genStumps(List<Instances> folds, int numStumps,
			double fraction, int depth) throws Exception {
		List<List<Id3>> stumpFolds = new ArrayList<List<Id3>>();
		Random random = new Random(System.currentTimeMillis());
		for (Instances insts : folds) {
			List<Id3> stumps = new ArrayList<Id3>();
			// 100 stumps for each fold
			for (int stumpCount = 0; stumpCount < numStumps; ++stumpCount) {
				Instances sampleInsts = new Instances(insts);
				// remove 50% of instances
				double numToRemove = Math.ceil(sampleInsts.numInstances() * fraction);
				for (int removeCount = 0; removeCount < numToRemove; ++removeCount) {
					int idxToRemove = random.nextInt(sampleInsts.numInstances());
					sampleInsts.delete(idxToRemove);
				}
				Id3 stump = new Id3();
				stump.setMaxDepth(depth);
				stump.buildClassifier(sampleInsts);
				stumps.add(stump);
			}
			stumpFolds.add(stumps);
		}
		return stumpFolds;
	}

	public static List<List<List<Double>>> genStumpVectors(
			List<List<Id3>> stumpFolds, List<Instances> folds) throws Exception {
		List<List<List<Double>>> stumpVectorFolds = new ArrayList<List<List<Double>>>();
		for (int foldIdx = 0; foldIdx < stumpFolds.size(); ++foldIdx) {
			List<Id3> stumpFold = stumpFolds.get(foldIdx);
			Instances insts = folds.get(foldIdx);
			List<List<Double>> vectors = new ArrayList<List<Double>>();
			for (int instIdx = 0; instIdx < insts.numInstances(); ++instIdx) {
				Instance inst = insts.instance(instIdx);
				List<Double> vector = stumpsToVector(stumpFold, inst);
				vectors.add(vector);
			}
			stumpVectorFolds.add(vectors);
		}
		return stumpVectorFolds;
	}

	public static List<Double> evalTree(List<Instances> trainingFolds,
			List<Instances> testFolds, int depth) throws Exception {
		List<Evaluation> evalSamples = new ArrayList<Evaluation>();
		List<Id3> treeSamples = new ArrayList<Id3>();
		for (int setIdx = 0; setIdx < trainingFolds.size(); ++setIdx) {
			Id3 tree = new Id3();
			Evaluation eval = new Evaluation(testFolds.get(setIdx));
			if (depth > 0) {
				tree.setMaxDepth(depth);
			}
			tree.buildClassifier(trainingFolds.get(setIdx));
			eval.evaluateModel(tree, testFolds.get(setIdx));
			evalSamples.add(eval);
			treeSamples.add(tree);
		}
		return getTreeResults(evalSamples, treeSamples);
	}

	public static List<Double> evalSGD(
			List<List<List<Double>>> trainingVectorFolds,
			List<List<List<Double>>> testVectorFolds, double convThres,
			double learnRate) {
		List<Double> samples = new ArrayList<Double>();
		int numFolds = trainingVectorFolds.size();
		for (int foldIdx = 0; foldIdx < numFolds; ++foldIdx) {
			List<Double> w = trainSGD(trainingVectorFolds.get(foldIdx), convThres,
					learnRate);
			List<Double> res = testSGD(w, testVectorFolds.get(foldIdx));
			samples.add(res.get(0) / res.get(1));
		}
		return getResults(samples);
	}

	public static List<Double> expTree(List<Instances> trainingFolds,
			List<Instances> testFolds) throws Exception {
		List<Double> results = new ArrayList<Double>(Arrays.asList(0.0, 0.0, 0.0));
		for (Integer depth = 0; depth <= 25; ++depth) {
			List<Double> result = evalTree(trainingFolds, testFolds, depth);
			if (result.get(0) > results.get(1)) { // greater accuracy
				results.set(0, depth.doubleValue()); // depth
				results.set(1, result.get(0)); // accuracy
				results.set(2, result.get(1)); // std dev
			}
		}
		return results;
	}

	public static List<Double> expSGD(
			List<List<List<Double>>> trainingVectorFolds,
			List<List<List<Double>>> testVectorFolds) {
		List<Double> results = new ArrayList<Double>(Arrays.asList(0.0, 0.0, 0.0,
				0.0));
		for (double convThres = 0.001; convThres <= 0.01; convThres += 0.001) {
			for (double learnRate = 0.01; learnRate <= 0.1; learnRate += 0.01) {
				List<Double> result = evalSGD(trainingVectorFolds, testVectorFolds,
						convThres, learnRate);
				if (result.get(0) > results.get(2)) { // greater accuracy
					results.set(0, convThres); // convThres
					results.set(1, learnRate); // learnRate
					results.set(2, result.get(0)); // accuracy
					results.set(3, result.get(1)); // std dev
				}
			}
		}
		return results;
	}

	public static List<Double> expSGDStumps(List<Instances> trainingFolds,
			List<Instances> testFolds) throws Exception {
		List<Double> results = new ArrayList<Double>(Arrays.asList(0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0));
		for (Integer numStumps = 100; numStumps <= 100; ++numStumps) {
			for (double fraction = 0.5; fraction <= 0.5; fraction += 0.05) {
				for (Integer stumpDepth = 4; stumpDepth <= 4; ++stumpDepth) {
					for (double convThres = 0.000001; convThres <= 0.00001; convThres += 0.000001) {
						for (double learnRate = 0.009; learnRate <= 0.011; learnRate += 0.001) {
							List<List<Id3>> stumpFolds = genStumps(trainingFolds, numStumps,
									fraction, stumpDepth);
							List<List<List<Double>>> trainingStumpFolds = genStumpVectors(
									stumpFolds, trainingFolds);
							List<List<List<Double>>> testStumpFolds = genStumpVectors(
									stumpFolds, testFolds);
							List<Double> result = evalSGD(trainingStumpFolds, testStumpFolds,
									convThres, learnRate);
							if (result.get(0) > results.get(5)) { // greater accuracy
								results.set(0, numStumps.doubleValue()); // numStumps
								results.set(1, fraction); // fraction
								results.set(2, stumpDepth.doubleValue()); // stumpDepth
								results.set(3, convThres); // convThres
								results.set(4, learnRate); // learnRate
								results.set(5, result.get(0)); // accuracy
								results.set(6, result.get(1)); // std dev
							}
						}
					}
				}
			}
		}
		return results;
	}

	public static void main(String[] args) throws Exception {
		if (args.length != 5) {
			System.err
					.println("Usage: Tester arff-fold1 arff-fold2 arff-fold3 arff-fold4 arff-fold5");
			System.exit(-1);
		}

		// read folds into memory
		List<Instances> folds = initialize(args);

		// create training and test folds
		List<Instances> trainingFolds = createTrainingFolds(folds);
		List<Instances> testFolds = createTestFolds(folds);

		// evaluate full tree
		List<Double> resultsFull = evalTree(trainingFolds, testFolds, 0);
		System.out.println("Full Tree Sample Mean Accuracy: " + resultsFull.get(0));
		System.out.println("Full Tree Sample Std Dev: " + resultsFull.get(1));
		System.out.println();

		// evaluate tree of depth 4
		List<Double> resultsStump4 = evalTree(trainingFolds, testFolds, 4);
		System.out.println("Depth Four Tree Sample Mean Accuracy: "
				+ resultsStump4.get(0));
		System.out.println("Depth Four Tree Sample Std Dev: "
				+ resultsStump4.get(1));
		System.out.println();

		// evaluate tree of depth 8
		List<Double> resultsStump8 = evalTree(trainingFolds, testFolds, 8);
		System.out.println("Depth Eight Tree Sample Mean Accuracy: "
				+ resultsStump8.get(0));
		System.out.println("Depth Eight Tree Sample Std Dev: "
				+ resultsStump8.get(1));
		System.out.println();

		// create vector representations of training and test folds
		List<List<List<Double>>> trainingVectorFolds = genVectors(trainingFolds);
		List<List<List<Double>>> testVectorFolds = genVectors(testFolds);

		List<Double> resultsSGD = expSGD(trainingVectorFolds, testVectorFolds);
		System.out.println("SGD Convergence Threshold: " + resultsSGD.get(0));
		System.out.println("SGD Learning Rate: " + resultsSGD.get(1));
		System.out.println("SGD Sample Mean Accuracy: " + resultsSGD.get(2));
		System.out.println("SGD Sample Std Dev: " + resultsSGD.get(3));
		System.out.println();

		List<Double> resultsSGDStumps = expSGDStumps(trainingFolds, testFolds);
		System.out.println("SGD Stumps Number of Stumps Used: "
				+ resultsSGDStumps.get(0));
		System.out.println("SGD Stumps Fraction of Data: "
				+ resultsSGDStumps.get(1));
		System.out.println("SGD Stumps Depth of Each Stump: "
				+ resultsSGDStumps.get(2));
		System.out.println("SGD Sample Convergence Threshold: "
				+ resultsSGDStumps.get(3));
		System.out.println("SGD Stumps Sample Learning Rate: "
				+ resultsSGDStumps.get(4));
		System.out.println("SGD Stumps Sample Mean Accuracy: "
				+ resultsSGDStumps.get(5));
		System.out.println("SGD Stumps Sample Std Dev: " + resultsSGDStumps.get(6));
		System.out.println();

		/*List<Double> resultsTree = expTree(trainingFolds, testFolds);
		System.out.println("Best Tree Depth: " + resultsTree.get(0));
		System.out.println("Best Tree Sample Mean Accuracy: " + resultsTree.get(1));
		System.out.println("Best Tree Sample Std Dev: " + resultsTree.get(2));
		System.out.println();*/
	}
}